let turn = 1;

function disableElement(elements){
	elements.map((x) => {
		document.getElementById("box" + x).disabled = true;
	})
}

function check_winner(){
	let box1, box2, box3, box4, box5, box6, box7, box8, box9; 
    box1 = document.getElementById("box1").value; 
    box2 = document.getElementById("box2").value; 
    box3 = document.getElementById("box3").value; 
    box4 = document.getElementById("box4").value; 
    box5 = document.getElementById("box5").value; 
    box6 = document.getElementById("box6").value; 
    box7 = document.getElementById("box7").value; 
    box8 = document.getElementById("box8").value; 
    box9 = document.getElementById("box9").value;

    if ((box1 == 'X') && (box2 == 'X') && (box3 == 'X')){
    	disableElement([4,5,6,7,8,9]);
	    window.alert('X is the winner'); 
	} 
	else if ((box1 == 'X') && (box4 == 'X') && (box7 == 'X')){
		disableElement([2,3,5,6,8,9]);
	    window.alert('X is the winner'); 
	} 
	else if ((box7 == 'X') && (box8 == 'X') && (box9 == 'X')){
		disableElement([1,2,3,4,5,6]);
	    window.alert('X is the winner'); 
	} 
	else if ((box3 == 'X') && (box6 == 'X') && (box9 == 'X')){
		disableElement([1,2,4,5,7,8]);
	    window.alert('X is the winner'); 
	} 
	else if ((box1 == 'X') && (box5 == 'X') && (box9 == 'X')){
		disableElement([2,3,4,6,7,8]);
	    window.alert('X is the winner'); 
	} 
	else if ((box3 == 'X') && (box5 == 'X') && (box7 == 'X')) {
		disableElement([1,2,4,6,8,9]); 
	    window.alert('X is the winner'); 
	} 
	else if ((box2 == 'X') && (box5 == 'X') && (box8 == 'X')) {
		disableElement([1,3,4,6,7,9]);
	    window.alert('X is the winner'); 
	} 
	else if ((box4 == 'X') && (box5 == 'X') && (box6 == 'X')) {
		disableElement([1,2,3,7,8,9]);
	    window.alert('X is the winner'); 
	}

	else if ((box1 == '0') && (box2 == '0') && (box3 == '0')) {
		disableElement([4,5,6,7,8,9]); 
        window.alert('0 is the winner'); 
    } 
    else if ((box1 == '0') && (box4 == '0') && (box7 == '0')) {
    	disableElement([2,3,5,6,8,9]); 
        window.alert('0 is the winner'); 
    } 
    else if (( box7 == '0') && (box8 == '0') && (box9 == '0')) {
    	disableElement([1,2,3,4,5,6]);
        window.alert('0 is the winner'); 
    } 
    else if ((box3 == '0') && (box6 == '0') && (box9 == '0')) { 
    	disableElement([1,2,4,5,7,8]);
        window.alert('0 is the winner'); 
    } 
    else if ((box1 == '0') && (box5 == '0') && (box9 == '0')) {
    	disableElement([2,3,4,6,7,8]); 
        window.alert('0 is the winner'); 
    } 
    else if ((box3 == '0') && (box5 == '0') && (box7 == '0')) { 
    	disableElement([1,2,4,6,8,9]);
        window.alert('0 is the winner'); 
    } 
    else if ((box2 == '0') && (box5 == '0') && (box8 == '0')) {
    	disableElement([1,3,4,6,7,9]);
        window.alert('0 is the winner'); 
    } 
    else if ((box4 == '0') && (box5 == '0') && (box6 == '0')) {
    	disableElement([1,2,3,7,8,9]);
        window.alert('0 is the winner'); 
    }
}

function reset() { 
    for(let i=1;i<=9;i++){
	    document.getElementById('box' + i).value = ''; 
	    document.getElementById('box' + i).disabled = false; 
    }
    window.alert('Reset board'); 
} 

function mark_box(num){
	if (turn == 1) { 
	    document.getElementById("box" + num).value = "X"; 
	    document.getElementById("box" + num).disabled = true; 
	    turn = 0; 
	} 
	else { 
	    document.getElementById("box" + num).value = "0"; 
	    document.getElementById("box" + num).disabled = true; 
	    turn = 1; 
	}
}
